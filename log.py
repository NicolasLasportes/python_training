import os.path
import subprocess
from var import log_path

def write_log(project_name, text):
    if os.path.exists(log_path + project_name + '.txt'):
        print("Log updated in " + log_path + project_name + ".txt")
        overwrite_or_append = "a"
    else:
        overwrite_or_append = "w" 
        print("The file didn't exist so we created it and wrote the log in, you can find it at " + log_path + project_name + ".txt")
    file = open(log_path + project_name + ".txt", overwrite_or_append)
    subprocess.call(['chmod', '-R', '777', log_path + project_name + ".txt"])
    file.write(text)
    file.close()