# imports
from log import write_log
from templates import *
from var import project_path
import os
import subprocess
import re

# functions
def write_in_file(file_to_write, text_to_write, append_or_overwrite):
    file = open(file_to_write, append_or_overwrite)
    file.write(text_to_write)
    file.close()

def check_project_name(project_name):
    if not re.match('^[\w-]+$', project_name):
        print("\033[31mProject name must be alphanumeric caracters only\033[0m")
        exit()

#checking if user runs script as root
if os.geteuid() != 0:
    write_log("vhost_generation", "This script needs to have root privileges\r\n")
    exit("\033[33mYou need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.\033[0m")

# variables
print("Enter project name")
project_name = input()
check_project_name(project_name)
print("This'll be the name of your project : ", project_name)
project_folder = project_path + project_name
print("This is the path of your project : ", project_folder)
apache_conf_file = "/etc/apache2/sites-available/" + project_name + ".conf"

add_host = "127.0.0.1 " + project_name + ".local"

VirtualHostTemplate = "<VirtualHost 127.0.0.1:80>\r\n" \
"   ServerName " + project_name + ".local\r\n" \
"   DocumentRoot " + project_folder + "\r\n" \
"   <Directory " + project_folder + ">\r\n" \
"       Options FollowSymLinks\r\n" \
"       AllowOverride ALL\r\n" \
"       Require all granted\r\n" \
"   </Directory>\r\n" \
"</VirtualHost>"

# main
try:
    os.mkdir(project_folder)
except OSError:
    print("\033[31mThere was an error on the creation of the folder\033[0m")
    write_log("vhost_generation", "Folder already exists\r\n")
    exit()

write_in_file(project_folder + "/index.html", htmlTemplate, "w")
subprocess.call(['chmod', '-R', '777', project_folder])

write_in_file(apache_conf_file, VirtualHostTemplate, "w")

write_in_file("/etc/hosts", add_host, "a")

subprocess.call(["a2ensite", project_name + ".conf"])
subprocess.call(["apache2ctl", "configtest"])
os.system('systemctl reload apache2')